//
//  SMAppDelegate.h
//  megogo
//
//  Created by Sergei Merenkov on 03/19/13.
//  Copyright (c) 2013 SergeiM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

//progress hud
+ (void)showProgress:(NSString *)title inView:(UIView *)view;
+ (void)hideProgressInView:(UIView *)view;

@end