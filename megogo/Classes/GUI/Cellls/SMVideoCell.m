//
//  SMVideoCell.m
//  megogo
//
//  Created by Sergei Merenkov on 19.03.13.
//  Copyright (c) 2013 SergeiM. All rights reserved.
//

#import "SMVideoCell.h"

@interface SMVideoCell ()
{
    IBOutlet __weak UILabel *_titleLabel;
    IBOutlet __weak UILabel *_subtitleLabel;
}

@end

@implementation SMVideoCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    //TODO: Implement CUSTOM background
    /*
    UIImageView *bgImageNormal = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"track_cell_bg~iphone.png"]];
    self.backgroundView = bgImageNormal;
    
    UIImageView *bgImageSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"active_track_cell_bg~iphone.png"]];
    self.selectedBackgroundView = bgImageSelected;
    */

    // Do not change text color when highlighted
    _titleLabel.highlightedTextColor = _titleLabel.textColor;
    _subtitleLabel.highlightedTextColor = _subtitleLabel.textColor;
}

#pragma mark Property setters
- (void)setVideoTitle:(NSString *)titleText
{
    _videoTitle = [titleText copy];
    _titleLabel.text = [titleText copy];
}

- (void)setArtistTitle:(NSString *)artistTitle
{
    _artistTitle = [artistTitle copy];
    _subtitleLabel.text = _artistTitle;
}

#pragma mark - Image Loader
- (void)didLoadImage:(UIImage *)image fromUrlString:(NSString *)urlString
{
    if (image)
    {
        self.pictureView.image = image;
    }
}

@end
