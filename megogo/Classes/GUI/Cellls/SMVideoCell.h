//
//  SMVideoCell.h
//  megogo
//
//  Created by Sergei Merenkov on 19.03.13.
//  Copyright (c) 2013 SergeiM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMImageLoader.h"

@interface SMVideoCell : UITableViewCell <SMImageLoaderProtocol>

@property (nonatomic, weak) IBOutlet UIImageView *pictureView;

@property (nonatomic, copy) NSString *videoTitle;
@property (nonatomic, copy) NSString *artistTitle;

@end
