//
//  SMListVideoVC.m
//  megogo
//
//  Created by Sergei Merenkov on 19.03.13.
//  Copyright (c) 2013 SergeiM. All rights reserved.
//

#import "SMListVideoVC.h"
#import "SMAppDelegate.h"
#import "SMTransmitter.h"
#import "SMVideoCell.h"

NSString *const kReuseTrackCellID = @"kReuseTrackCellID";

@interface SMListVideoVC () <UITableViewDataSource, UITableViewDelegate>{
    IBOutlet __weak UITableView *_tableView;
    NSMutableArray *_videos;
}
@end

@implementation SMListVideoVC

#pragma mark - Init methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
    }
    return self;
}

- (void)registerSuperNIBs
{
    NSBundle *videoCellClassBundle = [NSBundle bundleForClass:[SMVideoCell class]];

    UINib *trackCellNib = [UINib nibWithNibName:@"SMVideoCell" bundle:videoCellClassBundle];
    [_tableView registerNib:trackCellNib forCellReuseIdentifier:kReuseTrackCellID];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerSuperNIBs];
    _videos = @[];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [SMAppDelegate showProgress:nil inView:self.view];
    [[SMTransmitter sharedTransminner] listVideoTo:self
                                          selector:@selector(loadedVideoList:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table Datasource & Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_videos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SMVideoCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:kReuseTrackCellID forIndexPath:indexPath];

    //TODO: Implement Model for video
    NSDictionary *videoItem = (NSDictionary *)_videos[indexPath.row];

    cell.videoTitle = videoItem[@"title"];
    NSString *urlPoster = [NSString stringWithFormat:@"%@%@", APIURL, videoItem[@"poster"]];
    [[SMImageLoader sharedLoader] downloadImage:urlPoster to:cell];
    return cell;
}

#pragma mark -
- (void)loadedVideoList:(NSObject *)data {
    [SMAppDelegate hideProgressInView:self.view];
    _videos = [NSArray arrayWithArray:data];
    [_tableView reloadData];
}

@end
