//
//  SMImageLoader.m
//

#import "SMImageLoader.h"

const CGFloat kImageScale = 2;


#pragma mark - NSBlockOperation with weak userInfo

@interface NSBlockOperationWithWeakInfo : NSBlockOperation
@property(nonatomic, weak) id weakUserInfo;
@end

@implementation NSBlockOperationWithWeakInfo
@end

#pragma mark - RRImageLoader

@interface SMImageLoader ()
{
    NSMutableDictionary *_cacheForBundle;
    NSMutableDictionary *_cacheForDownloads;
    NSOperationQueue *_operationQueue;
    
    NSString *_deviceIdiom;
}

@end

@implementation SMImageLoader

#pragma mark - Static Methods

+ (SMImageLoader *)sharedLoader
{
    static dispatch_once_t once;
    static id sharedLoader;
    dispatch_once(&once, ^{
        sharedLoader = [[self alloc] init];
    });
    return sharedLoader;
}

#pragma mark - Init Methods

- (id)init
{
    self = [super init];
    if (self) {
        _cacheForBundle = [[NSMutableDictionary alloc] init];
        _cacheForDownloads = [[NSMutableDictionary alloc] init];
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 5;
    }
    return self;
}

#pragma mark - Public Methods

- (UIImage *)imageNamed:(NSString *)name
{
    NSAssert(name, @"Image name can't be nil");
    
    name = [name stringByAppendingString:[self deviceIdiom]];
    
    UIImage *image = [_cacheForBundle objectForKey:name];
    
    if(!image) {
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:name ofType:nil];
        image = [UIImage imageWithContentsOfFile:imagePath];
        
        if(image) {
            image = [UIImage imageWithCGImage:image.CGImage scale:kImageScale orientation:image.imageOrientation];
            
            [_cacheForBundle setValue:image forKey:name];
        } else {
            NSLog(@"\n*********ERROR********\nFILE %@ NOT FOUND", name);
        }
    }

    return image;
}

- (void)downloadImage:(NSString *)urlString to:(id<SMImageLoaderProtocol>)delegate useDiskCache:(BOOL)useDiskCache
{
    if (!urlString) {
        return;
    }
    
    __block UIImage *image = [_cacheForDownloads objectForKey:urlString];
    
    if (image) {
        [delegate didLoadImage:image fromUrlString:urlString];
    } else if(useDiskCache && [self isImageInCache:urlString]) {
        [self imageFromDiskCache:urlString to:delegate];
    }
    else {
        [self cancelDownloadsForDelegate:delegate];
        
        NSBlockOperationWithWeakInfo *downloadingOperation = [NSBlockOperationWithWeakInfo blockOperationWithBlock:^{
            image = [self imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
            
            if(image) {
                [_cacheForDownloads setValue:image forKey:urlString];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(){
                [delegate didLoadImage:image fromUrlString:urlString];
            });
        }];
        downloadingOperation.weakUserInfo = delegate;
        [_operationQueue addOperation:downloadingOperation];
    }
}

- (void)downloadImage:(NSString *)urlString to:(id<SMImageLoaderProtocol>)delegate
{
    [self downloadImage:urlString to:delegate useDiskCache:NO];
}

- (void)cancelDownloadsForDelegate:(id)delegate
{
    for (NSBlockOperationWithWeakInfo *operation in _operationQueue.operations) {
        if (delegate == operation.weakUserInfo) {
            [operation cancel];
        }
    }
}

- (void)preloadImage:(NSString *)urlString
{
    NSString *imageFilePath = [self cachePathForURL:urlString];
	UIImage *image = [UIImage imageWithContentsOfFile:imageFilePath];
    if (image) {
        [_cacheForDownloads setObject:image forKey:urlString];
    }
}

- (void)saveCacheForDonwloadsOnDisk
{
    for (NSString *urlString in [_cacheForDownloads allKeys]) {
        [self saveImageToCache:_cacheForDownloads[urlString] URLstring:urlString];
    }
}

- (void)clearCacheForDownloads
{
    [self saveCacheForDonwloadsOnDisk];
    [_cacheForDownloads removeAllObjects];
}

- (void)suspendDownloads:(BOOL)suspend
{
	[_operationQueue setSuspended:suspend];
}

- (void)cancelDownloadsInProgress
{
	[_operationQueue cancelAllOperations];
}

#pragma mark - Private Methods

- (UIImage *)imageWithData:(NSData*)imageData
{
    UIImage *image = [UIImage imageWithData:imageData];

    if (image)
        image = [UIImage imageWithCGImage:image.CGImage scale:kImageScale orientation:image.imageOrientation];
    
    return image;
}

- (NSString *)deviceIdiom
{
    if(!_deviceIdiom) {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            _deviceIdiom = @"~iphone.png";
        } else {
            _deviceIdiom = @"~ipad.png";
        }
    }
    return _deviceIdiom;
}

#pragma mark - Disk cache for downloaded images

- (NSString *)cachePath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSString *cachePath = paths[0];

	if (![[NSFileManager defaultManager] fileExistsAtPath:cachePath isDirectory:nil]) {
		NSError *error;
		[[NSFileManager defaultManager] createDirectoryAtPath:cachePath withIntermediateDirectories:NO attributes:nil error:&error];
		if (error) {
			NSLog(@"RRImageLoader can't create cache directory:%@", error.description);
			return nil;
		}
	}
	return cachePath;
}

- (NSString *)cachePathForURL:(NSString *)urlString
{
	NSString *validName = [urlString  stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
	NSString *imageFileName = [validName stringByAppendingPathExtension:@"png"];
	return [[self cachePath] stringByAppendingPathComponent:imageFileName];
}

- (BOOL)isImageInCache:(NSString *)urlString
{
	NSString *imageFilePath = [self cachePathForURL:urlString];
	return [[NSFileManager defaultManager] fileExistsAtPath:imageFilePath];
}

- (void)imageFromDiskCache:(NSString *)urlString to:(id <SMImageLoaderProtocol>)delegate
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^() {
	                       NSString *imageFilePath = [self cachePathForURL:urlString];
	                       UIImage *image = [UIImage imageWithContentsOfFile:imageFilePath];
	                       [_cacheForDownloads setObject:image forKey:urlString];
	                       dispatch_async(dispatch_get_main_queue(), ^() {
	                                              [delegate didLoadImage:image fromUrlString:urlString];
					      });
		       });
}

- (void)saveImageToCache:(UIImage *)image URLstring:(NSString *)urlString
{
	NSString *imageFilePath = [self cachePathForURL:urlString];
	NSData *imageData = UIImagePNGRepresentation(image);

	if (![self isImageInCache:urlString]) {
		bool success = [[NSFileManager defaultManager] createFileAtPath:imageFilePath contents:imageData attributes:nil];
		if (!success) {
			NSLog(@"SMImageLoader can't save image to cache. URL: %@ Path: %@", urlString, imageFilePath);
		}
	}
}

@end
