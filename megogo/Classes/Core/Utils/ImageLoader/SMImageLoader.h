//
//  SMImageLoader.h
//

#import <Foundation/Foundation.h>

@protocol SMImageLoaderProtocol <NSObject>

- (void)didLoadImage:(UIImage *)image fromUrlString:(NSString *)urlString;

@end

@interface SMImageLoader : NSObject

+ (SMImageLoader *)sharedLoader;

- (UIImage *)imageNamed:(NSString *) name;

- (void)downloadImage:(NSString *) URLString to:(id<SMImageLoaderProtocol>)delegate;    //useDiskCache = NO
- (void)downloadImage:(NSString *)urlString to:(id<SMImageLoaderProtocol>)delegate useDiskCache:(BOOL)useDiskCache;
- (void)preloadImage:(NSString *)urlString;

- (void)clearCacheForDownloads;
- (void)saveCacheForDonwloadsOnDisk;
- (void)suspendDownloads:(BOOL)suspend;
- (void)cancelDownloadsInProgress;

@end
