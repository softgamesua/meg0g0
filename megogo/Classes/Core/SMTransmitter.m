//
// Created by sergeym on 19.03.13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "SMTransmitter.h"
#import "SBJson.h"
#import <objc/message.h>

static BOOL allowInitializing = NO;

@interface SMTransmitter ()
- (void)getRequest:(NSString *)requestString to:(id)sender selector:(SEL)selector;
@end

@implementation SMTransmitter {
    NSOperationQueue *_queue;
}

+ (SMTransmitter *)sharedTransminner {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        allowInitializing = YES;
        sharedInstance = [[self alloc] init];
        allowInitializing = NO;
    });
    return sharedInstance;
}

- (id)init {
    NSAssert(allowInitializing == YES, @"Explicitly calling init method of SMTransmitter");

    self = [super init];
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
    }
    return self;
}

- (void)listVideoTo:(id)sender selector:(SEL)selector {
    NSString *urlString = [NSString stringWithFormat:@"%@/p/videos?category=4&genre=18&sign=01c17299740e8e380640631c57b2a859_fortest", APIURL]; //TODO: change the  link
    [self getRequest:urlString to:sender selector:selector];
}

- (void)getRequest:(NSString *)requestString to:(id)sender selector:(SEL)selector {
    requestString = [requestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
    [request setHTTPMethod:@"GET"];

    [NSURLConnection sendAsynchronousRequest:request queue:_queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(error) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
                [self failedRequest];
                objc_msgSend(sender, selector, nil);
            }];
            return;
        } else {
            id result = [data JSONValue];
            if (!result) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
                    [self failedRequest];
                    objc_msgSend(sender, selector, nil);
                }];
                return;
            }

            if ([result isKindOfClass:[NSDictionary class]]){
                if ([result[@"result"] isEqualToString:@"error"] || ![result[@"result"] isEqualToString:@"ok"]) {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
                        objc_msgSend(sender, selector, nil);
                    }];
                } else {
                    NSArray * convertedResult = result[@"video_list"];

                    NSLog(@"DATA = %@", [convertedResult description]);

                    [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
                        objc_msgSend(sender, selector, convertedResult);
                    }];
                }
            } else {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^() {
                    objc_msgSend(sender, selector, nil);
                }];
            }
        }
    }];
}

#pragma mark - messages
- (void)failedRequest
{
    NSLog(@"==== FAILED REQUEST ====");
}

@end